<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
    <h1>Nilai</h1>
    <?php
        function tentukan_nilai($number)
        {
            if($number <= 100 ) { $grade = "Sangat Baik<br>"; }
            if($number <  85 )  { $grade = "Baik<br>"; }
            if($number <  70 )  { $grade = "Cukup<br>"; }
            if($number <  60 )  { $grade = "Kurang<br>"; }
           
            return $grade;
           
            //  kode disini
        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang


    ?>
</body>
</html>