<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <?php
        function ubah_huruf($string){
        //kode di sini
        $len = strlen($string);

        for ($i = 0; $i < $len; ++$i) {
                    $string[$i] = chr(ord($string[$i]) + 1);
                }
            return $string;

            }
        
        // TEST CASES
        echo "<p>";
        echo ubah_huruf('wow'); // xpx
        echo "</p>";
        echo "<p>";
        echo ubah_huruf('developer'); // efwfmpqfs
        echo "</p>";
        echo "<p>";
        echo ubah_huruf('laravel'); // mbsbwfm
        echo "</p>";
        echo "<p>";
        echo ubah_huruf('keren'); // lfsfo
        echo "</p>";
        echo "<p>";
        echo ubah_huruf('semangat'); // tfnbohbu
        echo "<p>";

    ?>
</body>
</html>