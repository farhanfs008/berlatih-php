<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Besar kecil</title>
</head>
<body>
    <?php
    function tukar_besar_kecil($string){

        echo strtr($string, 
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    //kode di sini
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World<br>'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY<br>'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!<br>'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me<br>'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW<br>'); // "001-a-3-5tRDyw"

    ?>
</body>
</html>